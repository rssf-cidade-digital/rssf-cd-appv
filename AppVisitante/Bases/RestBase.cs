﻿using AppVisitante.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppVisitante.Bases
{
    public abstract class RestBase<T> where T: IDAO<T>
    {
        private readonly HttpClient _cliente;
        private readonly string _url;

        public RestBase(string url)
        {
            _cliente = new HttpClient();
            _url = url;
        }

        public async Task RetornarLista(string rota, ObservableCollection<T> contêiner)
        {
            var uri = RetornarURI(rota);
            var resulado = await _cliente.GetStringAsync(uri);
            var objetos = JsonConvert.DeserializeObject<List<T>>(resulado);

            foreach (var item in objetos)
            {
                contêiner.Add(item);
            }
        }

        public async Task<T> RetornarÚnico(string rota)
        {
            var uri = RetornarURI(rota);
            var resulado = await _cliente.GetStringAsync(uri);
            return JsonConvert.DeserializeObject<T>(resulado);
        }

        public async Task<T> EfetuarPost(string rota, IJsonificável corpo)
        {
            var uri = RetornarURI(rota);
            var resultado = await _cliente.PostAsync(uri, new StringContent(corpo.Jsonificar(), Encoding.UTF8, "application/json"));
            if(resultado.IsSuccessStatusCode)
            {
                var json = await resultado.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(json);
            }

            return default(T);
        }

        private string RetornarURI(string rota)
        {
            return $"{_url}/{rota}";
        }
    }
}
