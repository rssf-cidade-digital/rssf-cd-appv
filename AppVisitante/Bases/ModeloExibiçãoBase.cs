﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace AppVisitante.Bases
{
    public class ModeloExibiçãoBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _carregando;
        private bool _erroRede;
        private bool _normal;
        public bool Carregando
        {
            get => _carregando; protected set
            {
                _carregando = value;
                OnPropertyChanged();
            }
        }

        public bool ErroRede
        {
            get { return _erroRede; }

            protected set 
            { 
                _erroRede = value;
                OnPropertyChanged();
            }
        }

        public bool Normal
        {
            get { return _normal; }

            protected set 
            { 
                _normal = value;
                OnPropertyChanged();
            }
        }

        public ModeloExibiçãoBase()
        {
            Carregando = true;
            ErroRede = false;
            Normal = false;
        }

        public void OnPropertyChanged([CallerMemberName]string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public virtual void MostrarErroRede()
        {
            Carregando = false;
            ErroRede = true;
            Normal = false;
        }

        public virtual void MostrarNormal()
        {
            Carregando = false;
            ErroRede = false;
            Normal = true;
        }

        public virtual void MostrarCarregando()
        {
            Carregando = true;
            ErroRede = false;
            Normal = false;
        }
 
    }
}
