﻿using AppVisitante.DAO;
using AppVisitante.ModeloExibição;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppVisitante.Exibição
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelecionarCélula : ContentPage
    {
        public SelecionarCélulaModeloExibição ModeloExibição { get; set; }

        private readonly string _caminhoBaseDeDados;
        public SelecionarCélula(string caminhoBaseDeDados)
        {
            InitializeComponent();
            _caminhoBaseDeDados = caminhoBaseDeDados;
            Title = "Selecionar Célula";
            ModeloExibição = new SelecionarCélulaModeloExibição(_caminhoBaseDeDados);
            BindingContext = ModeloExibição;
       }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ModeloExibição.ListarCélulas();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void Células_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null) return;

            var célula = e.Item as SelecionarCélulaDAO;
            var decisão = await DisplayAlert("Célula", $"Selecionar essa célula: {célula.Endereço}?", "Sim", "Não");
            if(decisão)
            {
                ModeloExibição.SelecionarCélula(célula);
                await Navigation.PopAsync();
            }
        }
    }
}