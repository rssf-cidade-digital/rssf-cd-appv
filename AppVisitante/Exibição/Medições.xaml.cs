﻿
using AppVisitante.Exibição;
using AppVisitante.ModeloExibição;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppVisitante
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class Medições : ContentPage
    {
        private readonly string _caminhoBaseDeDados;
        public MediçõesModeloExibição ModeloExibição { get; set; }
        public Medições(string caminhoBaseDeDados)
        {
            InitializeComponent();
            _caminhoBaseDeDados = caminhoBaseDeDados;
            Title = "RSSF Cidade Digital";
            ModeloExibição = new MediçõesModeloExibição(caminhoBaseDeDados);
            BindingContext = ModeloExibição;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            FazerAssinaturas();
            await ModeloExibição.LerDados();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            CancelarAssinaturas();
        }

        private void CancelarAssinaturas()
        {
            MessagingCenter.Unsubscribe<MediçõesModeloExibição>(this, "SelecionarCélula");
            MessagingCenter.Unsubscribe<MediçõesModeloExibição>(this, "VerGráficos");
        }

        private void FazerAssinaturas()
        {
            MessagingCenter.Subscribe<MediçõesModeloExibição>(this, "SelecionarCélula", (objeto) =>
            {
                Navigation.PushAsync(new SelecionarCélula(_caminhoBaseDeDados));
            });

            MessagingCenter.Subscribe<MediçõesModeloExibição>(this, "VerGráficos", (objeto) =>
            {
                Navigation.PushAsync(new GerarGráficos(ModeloExibição.idCélula));
            });
        }

        private void Alertas_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null) return;

            if(sender is ListView lv) lv.SelectedItem = null;
        }
    }
}
