﻿using AppVisitante.Bases;
using AppVisitante.ModeloExibição;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppVisitante.Exibição
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class GerarGráficos : ContentPage
    {
        private readonly int _idCélula;
        public ModeloExibiçãoBase ModeloExibição { get; set; }


        public GerarGráficos(int idCélula)
        {
            InitializeComponent();
            Title = "Gerar Gráficos";
            _idCélula = idCélula;
            ModeloExibição = new GerarGráficosModeloExibição(idCélula);
            BindingContext = ModeloExibição;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
    }
}