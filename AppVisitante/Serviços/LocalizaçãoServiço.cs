﻿using AppVisitante.DAO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace AppVisitante.Serviços
{
    public static class LocalizaçãoServiço
    {
        public async static Task<LocalizaçãoDAO> RetornarLocalização()
        {
            var pedido = new GeolocationRequest(GeolocationAccuracy.Medium);
            var localização = await Geolocation.GetLocationAsync(pedido);

            var latitude = localização.Latitude.ToString().Replace(",", ".");
            var longitude = localização.Longitude.ToString().Replace(",", ".");

            return new LocalizaçãoDAO(latitude, longitude);
        }
    }
}
