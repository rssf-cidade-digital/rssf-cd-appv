﻿
using AppVisitante.Bases;
using AppVisitante.DAO;
using AppVisitante.Modelos;
using AppVisitante.REST;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace AppVisitante.ModeloExibição
{
    public class MediçõesModeloExibição : ModeloExibiçãoBase
    {
        private PáginaInicialDAO _dados;
        private readonly string _caminhoBaseDeDados;

        public int idCélula { get; set; }

        public ICommand SelecionarCélula { get; private set; }
        public ICommand VerGráficos { get; private set; }
        public PáginaInicialDAO Dados { get => _dados; set
            {
                _dados = value;
                OnPropertyChanged();
            }
        }
        public RestBase<PáginaInicialDAO> Rest { get; set; }
        public ObservableCollection<string> Alertas { get; set; }


        public MediçõesModeloExibição(string caminhoBaseDeDados)
        {
            _caminhoBaseDeDados = caminhoBaseDeDados;
            Rest = new PáginaInicialRest(Configurações.baseUrl);
            ConfigurarComandos();
            Dados = new PáginaInicialDAO();
            Alertas = new ObservableCollection<string>();
            AtualizarAlertas();
        }

        private void AtualizarAlertas()
        {
            Alertas.Clear();
            foreach (var item in Dados.Alertas)
            {
                Alertas.Add(item);
            }
        }

        public async Task LerDados()
        {
            CélulaSelecionadaModelo célula;
            using (var contexto = new AplicaçãoContext(_caminhoBaseDeDados))
            {
                célula = contexto.Set<CélulaSelecionadaModelo>().FirstOrDefault();
            }

            if (célula != null)
            {
                try
                {
                    idCélula = célula.IdCélula;
                    Dados = await Rest.RetornarÚnico($"celulas/dados/{célula.IdCélula}");
                    AtualizarAlertas();
                    MostrarNormal();
                }
                catch (WebException)
                {
                    MostrarErroRede();
                }
            }
        }

        private void ConfigurarComandos()
        {
            SelecionarCélula = new Command(async () =>
            {
                MessagingCenter.Send(this, "SelecionarCélula");
            });

            VerGráficos = new Command(async () =>
            {
                MessagingCenter.Send(this, "VerGráficos");
            });
        }
    }
}
