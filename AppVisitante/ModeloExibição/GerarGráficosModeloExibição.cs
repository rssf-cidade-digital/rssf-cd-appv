﻿using AppVisitante.Bases;
using AppVisitante.DAO;
using AppVisitante.REST;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AppVisitante.ModeloExibição
{
    public class GerarGráficosModeloExibição : ModeloExibiçãoBase
    {

        private string _gráfico;
        private int _altura;
        private bool _mostrarGráfico;
        private string _nomeGráfico;
        
        private readonly int _idCélula;

        public string Gráfico
        {
            get { return _gráfico; }
            set 
            {
                _gráfico = value;
                OnPropertyChanged();
            }
        }


        public int Altura
        {
            get { return _altura; }
            set 
            { 
                _altura = value;
                OnPropertyChanged();
            }
        }

        private bool _fazendoDownload;

        public bool FazendoDownload
        {
            get { return _fazendoDownload; }
            set 
            { 
                _fazendoDownload = value;
                OnPropertyChanged();
            }
        }



        public bool MostrarGráfico
        {
            get { return _mostrarGráfico; }
            set 
            { 
                _mostrarGráfico = value;
                OnPropertyChanged();
            }
        }

        public ICommand GerarGráfico { get; set; }
        public ICommand Download { get; set; }
        public ICommand Compartilhar { get; set; }

        public int TipoSelecionado { get; set; }
        public DateTime DataInício { get; set; }
        public DateTime DataFim { get; set; }
        public TimeSpan HoraInício { get; set; }
        public TimeSpan HoraFim { get; set; }
        public string[] Tipos { get; set; }


        public RestBase<ImagemDAO> Rest { get; set; }

        public GerarGráficosModeloExibição(int idCélula) : base()
        {
            _idCélula = idCélula;
            MostrarNormal();
            ConfigurarComandos();
            Rest = new ImagemRest(Configurações.pythonUrl);
            Tipos = new string[]
            {
                "Temperatura",
                "Umidade",
                "Radiação UV",
                "Gases Tóxicos",
                "Fumaça",
                "Incêndio"
            };

            DataInício = DateTime.Now;
            DataFim = DateTime.Now.AddYears(1);
        }

        public override void MostrarCarregando()
        {
            base.MostrarCarregando();
            MostrarGráfico = false;
            FazendoDownload = false;
        }

        public override void MostrarErroRede()
        {
            base.MostrarErroRede();
            MostrarGráfico = false;
            FazendoDownload = false;
        }

        public override void MostrarNormal()
        {
            base.MostrarNormal();
            MostrarGráfico = false;
            FazendoDownload = false;
        }

        public void ExibirGráfico()
        {
            Normal = true;
            Carregando = false;
            ErroRede = false;
            MostrarGráfico = true;
            FazendoDownload = false;
        }

        public void ExibirFazendoDownload()
        {
            Normal = true;
            Carregando = false;
            ErroRede = false;
            MostrarGráfico = true;
            FazendoDownload = true;
        }

        private async Task BaixarImagem()
        {
            ExibirFazendoDownload();

            var clieneHttp = new HttpClient();
            byte[] imagem;

            using(var resposta = await clieneHttp.GetAsync(Gráfico))
            {
                if(resposta.IsSuccessStatusCode)
                {
                    string caminho = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                    string caminhoCompleto = Path.Combine(caminho, _nomeGráfico);

                    imagem = await resposta.Content.ReadAsByteArrayAsync();
                    File.WriteAllBytes(caminhoCompleto, imagem);
                }
            }

            ExibirGráfico();
        }

        private void ConfigurarComandos()
        {
            GerarGráfico = new Command(async () =>
            {
                MostrarCarregando();

                var dao = new FiltroTemporalDAO(DataInício, HoraInício, DataFim, HoraFim);
                try
                {
                    var resposta = await Rest.EfetuarPost($"{TipoSelecionado + 1}/{_idCélula}", dao);
                    Gráfico = $"{Configurações.HttpEstáticoUrl}/{resposta.Arquivo}";
                    _nomeGráfico = resposta.Arquivo;

                    Altura = 730;
                    ExibirGráfico();
                }
                catch (OperationCanceledException)
                {
                    MostrarErroRede();
                }

            });

            Download = new Command(async() => await BaixarImagem());

            Compartilhar = new Command(async () =>
            {
                await BaixarImagem();
                var arquivo = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), _nomeGráfico);
                await Share.RequestAsync(new ShareFileRequest()
                {
                    Title = "Compartilhar Gráfico",
                    File = new ShareFile(arquivo)
                });
            });
        }
    }
}
