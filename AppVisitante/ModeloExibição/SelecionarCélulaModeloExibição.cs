﻿using AppVisitante.Bases;
using AppVisitante.DAO;
using AppVisitante.Modelos;
using AppVisitante.REST;
using AppVisitante.Serviços;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AppVisitante.ModeloExibição
{
    public class SelecionarCélulaModeloExibição : ModeloExibiçãoBase
    {
        public RestBase<SelecionarCélulaDAO> Rest { get; private set; }
        public ObservableCollection<SelecionarCélulaDAO> Células { get; set; }

        private readonly string _caminhoBaseDeDados;

        public SelecionarCélulaModeloExibição(string caminhoBaseDeDados) : base()
        {
            _caminhoBaseDeDados = caminhoBaseDeDados;
            Células = new ObservableCollection<SelecionarCélulaDAO>();
            Rest = new SelecionarCélulaRest(Configurações.baseUrl);
        }

        public async Task ListarCélulas()
        {
            var localização = await LocalizaçãoServiço.RetornarLocalização();
            try
            {
                await Rest.RetornarLista($"celulas/listar/{localização.Latitude}/{localização.Longitude}", Células);
                MostrarNormal();
            }
            catch (WebException)
            {
                MostrarErroRede();
            }
        }

        public void SelecionarCélula(SelecionarCélulaDAO célula)
        {
            using(var contexto = new AplicaçãoContext(_caminhoBaseDeDados))
            {
                var database = contexto.Set<CélulaSelecionadaModelo>();
                var selecionado = database.FirstOrDefault();
                if(selecionado == null)
                {
                    var novoSelecionado = new CélulaSelecionadaModelo() { IdCélula = célula.Id };
                    database.Add(novoSelecionado);
                }
                else
                {
                    selecionado.IdCélula = célula.Id;
                }

                contexto.SaveChanges();
            }
        }
    }
}
