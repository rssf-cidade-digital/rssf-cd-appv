﻿using AppVisitante.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppVisitante.DAO
{
    public class SelecionarCélulaDAO : IDAO<SelecionarCélulaDAO>
    {
        public int Id { get; set; }
        public string Endereço { get; set; }
        public string Distância { get; set; }

        public string TextoFrontEnd { get => $"Célula {Id} - {Endereço}, a {Distância} de distância"; }

        public SelecionarCélulaDAO(int iD, string endereço, string distância)
        {
            Id = iD;
            Endereço = endereço;
            Distância = distância;
        }

        public void Atualizar(SelecionarCélulaDAO objeto)
        {
            Id = objeto.Id;
            Endereço = objeto.Endereço;
            Distância = objeto.Distância;
        }
    }
}
