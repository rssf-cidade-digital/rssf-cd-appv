﻿using AppVisitante.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppVisitante.DAO
{
    public class FiltroTemporalDAO : IJsonificável
    {
        public string Início { get; set; }
        public string Fim { get; set; }

        public FiltroTemporalDAO(DateTime dataInício, TimeSpan horaInício, DateTime dataFim, TimeSpan horaFim)
        {
            var momentoInício = dataInício.Add(horaInício);
            var momentoFim = dataFim.Add(horaFim);

            Início = $"{momentoInício:yyyy-MM-dd HH:mm:ss}";
            Fim = $"{momentoFim:yyyy-MM-dd HH:mm:ss}";
        }
        public string Jsonificar()
        {
            return JsonConvert.SerializeObject(this).ToLower();
        }
    }
}
