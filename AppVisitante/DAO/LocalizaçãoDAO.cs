﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppVisitante.DAO
{
    public class LocalizaçãoDAO
    {
        public LocalizaçãoDAO(string latitude, string longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
