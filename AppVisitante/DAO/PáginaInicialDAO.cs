﻿using AppVisitante.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppVisitante.DAO
{
    public class PáginaInicialDAO : IDAO<PáginaInicialDAO>
    {
        public string EndereçoCélula { get; set; }
        public string ÚltimaLeituraCélula { get; set; }
        public int Temperatura { get; set; }
        public int Umidade { get; set; }
        public int UV { get; set; }
        public bool TemperaturaAlerta { get; set; }
        public bool UmidadeAlerta { get; set; }
        public bool UVAlerta { get; set; }
        public bool Gás { get; set; }
        public bool Fumaça { get; set; }
        public bool Fogo { get; set; }
        public string[] Alertas { get; set; }


        public string TextoTemperatura { get => $"{Temperatura}°C"; }
        public string CorTemperatura { get => RetornarCor(TemperaturaAlerta); }

        public string TextoUmidade { get => $"{Umidade}%"; }
        public string CorUmidade { get => RetornarCor(UmidadeAlerta); }

        public string TextoUV { get => $"{UV} UVi"; }
        public string CorUV { get => RetornarCor(UVAlerta); }

        public string TextoFumaça { get => RetornarEstado(Fumaça); }
        public string CorFumaça { get => RetornarCor(Fumaça); }

        public string TextoGásTóxico { get => RetornarEstado(Gás); }
        public string CorGásTóxico { get => RetornarCor(Gás); }

        public string TextoFogo { get => RetornarEstado(Fogo); }
        public string CorFogo { get => RetornarCor(Fogo); }

        public string TextoÚltimaLeituraCélula { get => $"Última Leitura: {ÚltimaLeituraCélula}"; }


        public PáginaInicialDAO() : this("Selecione uma célula.", "00/00 - 00:00", 0, 0, 0, true, true, true,
                true, true, true, new string[]{"Selecione uma célula."})
        {

        }

        public PáginaInicialDAO(string endereçoCélula, string últimaLeituraCélula, int temperatura,
            int umidade, int uV, bool temperaturaAlerta, bool umidadeAlerta, bool uvAlerta, bool gás,
            bool fumaça, bool fogo, string[] alertas)
        {
            EndereçoCélula = endereçoCélula;
            ÚltimaLeituraCélula = últimaLeituraCélula;
            Temperatura = temperatura;
            Umidade = umidade;
            TemperaturaAlerta = temperaturaAlerta;
            UmidadeAlerta = umidadeAlerta;
            UVAlerta = uvAlerta;
            UV = uV;
            Gás = gás;
            Fumaça = fumaça;
            Fogo = fogo;
            Alertas = alertas;
        }

        private string RetornarEstado(bool flag)
        {
            if (flag) return "Sim";
            return "Não";
        }

        private string RetornarCor(bool flag)
        {
            if (flag) return "Red";
            return "Green";
        }

        public void Atualizar(PáginaInicialDAO objeto)
        {
            Temperatura = objeto.Temperatura;
            Umidade = objeto.Umidade;
            UV = objeto.UV;
            TemperaturaAlerta = objeto.TemperaturaAlerta;
            UmidadeAlerta = objeto.TemperaturaAlerta;
            UVAlerta = objeto.UVAlerta;
            Gás = objeto.Gás;
            Fumaça = objeto.Fumaça;
            Fogo = objeto.Fogo;
            Alertas = objeto.Alertas;
        }
    }
}
