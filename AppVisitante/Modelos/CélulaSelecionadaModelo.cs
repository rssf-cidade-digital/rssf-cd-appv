﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppVisitante.Modelos
{
    public class CélulaSelecionadaModelo
    {
        public int Id { get; set; }
        public int IdCélula { get; set; }
    }
}
