﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppVisitante.Interfaces
{
    public interface IJsonificável
    {
        public string Jsonificar();
    }
}
