﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppVisitante.Interfaces
{
    public interface IDAO<T>
    {
        public void Atualizar(T objeto);
    }
}
