﻿using AppVisitante.Bases;
using AppVisitante.DAO;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppVisitante.REST
{
    public class SelecionarCélulaRest : RestBase<SelecionarCélulaDAO>
    {
        public SelecionarCélulaRest(string url) : base(url)
        {
        }
    }
}
