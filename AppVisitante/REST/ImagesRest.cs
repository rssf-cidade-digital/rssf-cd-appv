﻿using AppVisitante.Bases;
using AppVisitante.DAO;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppVisitante.REST
{
    public class ImagemRest : RestBase<ImagemDAO>
    {
        public ImagemRest(string url) : base(url)
        {
        }
    }
}
