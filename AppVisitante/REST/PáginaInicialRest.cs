﻿using AppVisitante.Bases;
using AppVisitante.DAO;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppVisitante.REST
{
    public class PáginaInicialRest : RestBase<PáginaInicialDAO>
    {
        public PáginaInicialRest(string url) : base(url)
        {
        }
    }
}
