﻿using AppVisitante.Modelos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppVisitante
{
    public class AplicaçãoContext : DbContext
    {
        public DbSet<CélulaSelecionadaModelo> CélulaSelecionada { get; set; }

        private readonly string _caminhoBaseDeDados;

        public AplicaçãoContext(string caminhoBaseDeDados)
        {
            _caminhoBaseDeDados = caminhoBaseDeDados;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CélulaSelecionadaModelo>().HasKey(c => c.Id);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlite($"Filename={_caminhoBaseDeDados}");
        }
    }
}
