﻿using AppVisitante.Exibição;
using Microsoft.EntityFrameworkCore;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppVisitante
{
    public partial class App : Application
    {
        public App(string caminhoBaseDeDados)
        {
            InitializeComponent();

            using(var contexto = new AplicaçãoContext(caminhoBaseDeDados))
            {
                contexto.Database.EnsureCreated();
                contexto.Database.ExecuteSqlRaw(Configurações.sqlInicial);
            }

            MainPage = new NavigationPage(new Medições(caminhoBaseDeDados));
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
