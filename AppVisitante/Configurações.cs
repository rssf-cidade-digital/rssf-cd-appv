﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppVisitante
{
    public static class Configurações
    {
        public static readonly string baseUrl = "http://170.246.105.251:5000";
        public static readonly string sqlInicial = "create table if not exists CélulaSelecionada(" +
            "Id integer not null primary key autoincrement," +
            "idCélula integer not null);";

        public static readonly string pythonUrl = "http://170.246.105.251:5300";
        public static readonly string HttpEstáticoUrl = "http://170.246.105.251:8081";
    }
}
